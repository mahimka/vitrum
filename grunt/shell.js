module.exports = {
    options: {
        failOnError: false
    },

    //////////////////////////// git
    git_add: {
        command: 'git add .'
    },

    git_commit: {
        command: 'git commit -a -m ' + new Date().toISOString()
    },

    git_push: {
        command: 'git push origin master'
    },

    //////////////////////////// webpack
    live: {
        command: 'webpack-dev-server --inline --hot --color'
    }

};