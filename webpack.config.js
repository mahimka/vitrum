'use strict';

const LANG = 'en';
const NODE_ENV = process.env.NODE_ENV || 'pro';
const HtmlWebpackPlugin = require('html-webpack-plugin')
const CleanWebpackPlugin = require('clean-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const webpack = require('webpack');
const rimraf = require('rimraf');
const jade = require("jade");
const nib = require("nib");

console.log(NODE_ENV, LANG);

module.exports = {

    entry: {
        main: ['./src/main']
    },

    output: {
        path: "www",
        filename: addHash('[name].js', 'hash'),
        library: "main"
    },

    devtool: NODE_ENV == 'dev' ? 'cheap-inline-module-source-map' : null,

    plugins: [
        new CopyWebpackPlugin([
            {
                from: './src/static/img',
                to: './static/img'
            },
            {
                from: './src/static/video',
                to: './static/video'
            },            
            // {
            //     from: './src/static/fonts',
            //     to: './static/fonts'
            // },
            {
                from: './src/modernizr.js',
                to: './modernizr.js'
            }
        ]),
        new CleanWebpackPlugin(['www'], {
            verbose: true,
            dry: false
            // exclude: ['static']
        }),
        new HtmlWebpackPlugin({
            templateContent: function(){ return jade.compileFile('./src/index.jade', {pretty:true});}(),
            watch:'./src/index.jade',
            inject: 'body',
            lang: LANG
        }),
        new webpack.NoErrorsPlugin(),
        new webpack.DefinePlugin({
            NODE_ENV: JSON.stringify(NODE_ENV),
            LANG: JSON.stringify(LANG)
        })
    ],

    resolve: {
        alias: {
            'TweenLite': 'gsap/src/uncompressed/TweenLite.js',
            'TweenMax': 'gsap/src/uncompressed/TweenMax.js',
            'TimelineLite': 'gsap/src/uncompressed/TimelineLite.js',
            'TimelineMax': 'gsap/src/uncompressed/TimelineMax.js',
            'CSSPlugin': 'gsap/src/uncompressed/plugins/CSSPlugin.js',
            "ScrollMagic": 'scrollmagic/scrollmagic/uncompressed/ScrollMagic.js',
            "animation.gsap": 'scrollmagic/scrollmagic/uncompressed/plugins/animation.gsap.js',
            "debug.addIndicators": 'scrollmagic/scrollmagic/uncompressed/plugins/debug.addIndicators.js'
        },
        modulesDirectories: ['node_modules'],
        extensions: ['', '.js', '.styl']
    },

    resolveLoader: {
        modulesDirectories: ['node_modules'],
        moduleTemplates: ['*-loader', '*'],
        extensions: ['', '.js', '.styl']
    },

    module: {
        loaders: [
            {test: /\.js$/, exclude: /(node_modules|bower_components)/, loader: 'babel?optional[]=runtime'},
            {test: /\.(pug|jade)$/, exclude: /(node_modules|bower_components)/, loader: 'pug'},
            {test: /\.css$/, exclude: /(node_modules|bower_components)/, loader: 'style!css!autoprefixer?browsers=last 2 versions'},
            {test: /\.styl$/, exclude: /(node_modules|bower_components)/, loader: 'style!css!autoprefixer?browsers=last 2 versions!stylus?resolve url'},
            {test: /\.json$/, exclude: /(node_modules|bower_components)/, loader: 'json'},
            {test: /\.csv$/, exclude: /(node_modules|bower_components)/, loader: 'dsv'},
            //{test: /\.(png|jpg|jpeg|gif|svg)$/, exclude: /(src\/static\/fonts)/, loader: addHash('file?name=static/img/[name].[ext]', 'hash:6')},
            {test: /\.(eot|svg|ttf|woff|woff2)$/, loader: 'file?name=static/fonts/[name].[ext]'}
        ]
    },

    stylus: {
        use: [nib()],
        define: {
            lang: function(){return LANG}
        }
    },

    devServer: {
        host: 'localhost',
        port: 8080,
        outputPath: 'www'
    }

}

function addHash(template, hash) {
    return NODE_ENV == 'pro' ? template.replace(/\.[^.]+$/, `.[${hash}]$&`) : `${template}?hash=[${hash}]`;
}

if(NODE_ENV=='pro'){
    module.exports.plugins.push(
        { apply: (compiler) => { rimraf.sync(compiler.options.output.path);}}
    );
    module.exports.plugins.push(
        new webpack.optimize.UglifyJsPlugin({
            compress: {
                warnings: false,
                drop_console: true,
                unsafe: true
            }
        })
    );
}