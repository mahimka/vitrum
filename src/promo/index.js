'use strict';

import './promo.styl';
import template from './promo.jade'
import { EventEmitter } from 'events'

export default class Promo extends EventEmitter{
    constructor(options) {
        super();

        this.elem = document.createElement('div');
        this.elem.className = 'promo';
        this.elem.innerHTML = template();
    }
};