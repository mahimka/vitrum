'use strict';

import './info.styl';
import template from './info.jade'
import { EventEmitter } from 'events'
import $ from 'jquery'

let workAdviceN = 0;
let homeAdviceN = 0;
let workLength = 0, homeLength = 0;

export default class Info extends EventEmitter{
    constructor(options) {
        super();

        this.elem = document.createElement('div');
        this.elem.className = 'info';
        this.elem.innerHTML = template();
    }

    init(){

        let i = 0, j = 0;
        for(let el of window.infoData){
            if(i>1){
                if(j==0){
                    if(el['home'] != ''){
                        homeLength++
                        $('#infop1').find('.paginator').append('<div class="adv"></div>')
                    }
                    if(el['work'] != ''){
                        workLength++
                        $('#infop2').find('.paginator').append('<div class="adv"></div>')
                    }
                    j++
                }else{
                    j = 0
                }
            }
            i++;
        }

        for(let k=0; k<$('#infop1').find('.adv').length; k++){
            $($('#infop1').find('.adv')[k]).data('id', k)
        }
        for(let m=0; m<$('#infop2').find('.adv').length; m++){
            $($('#infop2').find('.adv')[m]).data('id', m)
        }
        
        
        $('#infop1').find('.adv').on('click', (e)=>{
            homeAdviceN = $(e.target).data('id')
            this.setAdvice(homeAdviceN, $('#infop1'), 'home')
        })
        $('#infop2').find('.adv').on('click', (e)=>{
            workAdviceN = $(e.target).data('id')
            this.setAdvice(workAdviceN, $('#infop2'), 'work')
        })

        $('#infop1').find('.left_arrow').on('click', (e)=>{
            homeAdviceN--;
            if(homeAdviceN < 0)
                homeAdviceN = homeLength-1
            this.setAdvice(homeAdviceN, $('#infop1'), 'home')
        })
        $('#infop1').find('.right_arrow').on('click', (e)=>{
            homeAdviceN++;
            if(homeAdviceN >= homeLength)
                homeAdviceN = 0
            this.setAdvice(homeAdviceN, $('#infop1'), 'home')
        })

        $('#infop2').find('.left_arrow').on('click', (e)=>{
            workAdviceN--;
            if(workAdviceN < 0)
                workAdviceN = workLength-1
            this.setAdvice(workAdviceN, $('#infop2'), 'work')
        })
        $('#infop2').find('.right_arrow').on('click', (e)=>{
            workAdviceN++;
            if(workAdviceN >= workLength)
                workAdviceN = 0
            this.setAdvice(workAdviceN, $('#infop2'), 'work')
        })

        $('#home').find('.nextbtn').on('click', (e)=>{
            $("html, body").stop().animate({scrollTop: $('#infop1').offset().top - 150}, '3000', 'swing', function() {});

            window.dataLayer = window.dataLayer || [];
            dataLayer.push({
                event: 'link_button',
                pagePath: 'slide_2',
                slide_title: 'vitrum - 02',
                anchor: 'anchor-01',
                project: 'vitrum',
                mode: 'flat',
                version: 'd',
                link_button: 'home_click'
            })
        })
        $('#work').find('.nextbtn').on('click', (e)=>{
            $("html, body").stop().animate({scrollTop: $('#infop2').offset().top - 150}, '3000', 'swing', function() {});

            window.dataLayer = window.dataLayer || [];
            dataLayer.push({
                event: 'link_button',
                pagePath: 'slide_2',
                slide_title: 'vitrum - 02',
                anchor: 'anchor-01',
                project: 'vitrum',
                mode: 'flat',
                version: 'd',
                link_button: 'work_click'
            })
        })

        $('#quick0').on('click', (e)=>{
            $("html, body").stop().animate({scrollTop: $('#infop1').offset().top - 150}, '3000', 'swing', function() {});

            window.dataLayer = window.dataLayer || [];
            dataLayer.push({
                event: 'link_button',
                pagePath: 'slide_2',
                slide_title: 'vitrum - 02',
                anchor: 'anchor-01',
                project: 'vitrum',
                mode: 'flat',
                version: 'd',
                link_button: 'homeSlide_click'
            })
        })
        $('#quick1').on('click', (e)=>{
            $("html, body").stop().animate({scrollTop: $('#infop2').offset().top - 150}, '3000', 'swing', function() {});

            window.dataLayer = window.dataLayer || [];
            dataLayer.push({
                event: 'link_button',
                pagePath: 'slide_2',
                slide_title: 'vitrum - 02',
                anchor: 'anchor-01',
                project: 'vitrum',
                mode: 'flat',
                version: 'd',
                link_button: 'workSlide_click'
            })
        })

        $('#infop1').find('.infoblock_header').html(window.infoData[1].home)
        $('#infop2').find('.infoblock_header').html(window.infoData[1].work)

        this.setAdvice(homeAdviceN, $('#infop1'), 'home')
        this.setAdvice(workAdviceN, $('#infop2'), 'work')

        $(document).on( 'scroll', onScroll);
        
        function onScroll(){
            let y = window.pageYOffset
            let hg = window.innerHeight
            if(y + hg/2 - 90> 705){
                $('.quicklinks').css('top', y + hg/2 - 90 + 'px')
            }else{
                $('.quicklinks').css('top', 705 + 'px')
            }
        }
    }

    setAdvice(n, el, type){
        el.find('.info_advice_header').hide()
        el.find('.info_advice_text').hide()
        el.find('.info_advice_header').html(window.infoData[2+2*n][type])
        el.find('.info_advice_text').html(window.infoData[3+2*n][type])
        el.find('.adv').removeClass('selected');
        $(el.find('.adv')[n]).addClass('selected');
        el.find('.info_advice_header').fadeIn()
        el.find('.info_advice_text').fadeIn()

        window.dataLayer = window.dataLayer || [];
        dataLayer.push({
            event : 'generic',
            pagePath: 'slide_2',
            slide_title: 'vitrum - 2',
            anchor: 'anchor-01',
            object: type,
            object_event: type + n                                                                 // Название события
        })
    }

    anc(){
        let once0 = false;
        let once1 = false;
        let once2 = false;
        let once3 = false;
        $(document).scroll(function () {
            anchor();
        });
        anchor();

        function anchor(){
            let s_top = $("body").scrollTop();
            let hg = window.innerHeight;

            let yes0 = $(".infoblock2").find(".infoblock_header").offset().top;
            let yes1 = $(".infoblock2").find(".infoadvicesblock").offset().top;
            let yes2 = $(".infoblock1").find(".infoblock_header").offset().top;
            let yes3 = $(".infoblock1").find(".infoadvicesblock").offset().top;

            if(hg + s_top > yes0){
                if(!once0){
                    once0 = true
                    window.dataLayer = window.dataLayer || [];
                    dataLayer.push({
                        event : 'generic',
                        pagePath: 'slide_2',
                        slide_title: 'vitrum - 2',
                        anchor: 'anchor0301',
                        object: 'anchor0301',
                        object_event: 'anchor0301_show'
                    })
                }
            }

            if(hg + s_top > yes2){
                if(!once2){
                    once2 = true
                    window.dataLayer = window.dataLayer || [];
                    dataLayer.push({
                        event : 'generic',
                        pagePath: 'slide_2',
                        slide_title: 'vitrum - 2',
                        anchor: 'anchor0302',
                        object: 'anchor0302',
                        object_event: 'anchor0302_show'
                    })
                }
            }
        }
    }

    onn(){
        $(this.elem).fadeIn()
    }

    off(){
        $(this.elem).hide()
    }  
};