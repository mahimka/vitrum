'use strict';

import 'normalize-css'
import './main.styl';
import $ from 'jquery'
const ScrollMagic = require('ScrollMagic');
require('animation.gsap');
require('debug.addIndicators');
const TweenMax = require('TweenMax');

import Menu from './menu'
import Intro from './intro'
import Product from './product'
import Info from './info'
import Test from './test'

import productData from './data/product.csv'
import infoData from './data/info.csv'
import testData from './data/test.csv'

let menu; 
let intro, product, info, test;
let parts = [];

let currentPart = 0;

function ready(){

    window.productData = productData;
    window.infoData = infoData;
    window.testData = testData;

    menu = new Menu();
    document.querySelector('body').appendChild(menu.elem);
    menu.init(currentPart);
    menu.on('changePart', (id)=>{

        ga1(currentPart, id);

        let isShow = true
        if((currentPart == 1)&&(id == 4))
            isShow = false
        currentPart = (id!=4) ? id : 1;
        if(id == 4)
            menu.press(1)
        if(isShow)
        partOn();
        if(id == 4){
            $("html, body").stop().animate({scrollTop:$('.promo').offset().top - 200}, '3000', 'swing', function() {});
        }
    })

    intro = new Intro();
    document.querySelector('.content').appendChild(intro.elem);
    intro.init();
    intro.on('selectCommaRight', ()=>{
        currentPart = 1;
        menu.press(1)
        partOn();

        window.dataLayer = window.dataLayer || [];
        dataLayer.push({
            event: 'link_button',
            pagePath: 'slide_0',
            slide_title: 'vitrum - 0',
            anchor: 'anchor-01',
            project: 'vitrum',
            mode: 'flat',
            version: 'd',
            link_button: 'comma1_click'
        })
        dataLayer.push({
            event: 'pageview',
            pagePath: 'slide_' + currentPart,
            slide_title: 'vitrum - ' + currentPart,
            anchor: 'anchor-01',
            project: 'vitrum',
            mode: 'flat',
            version: 'd'
        })
    })
    intro.on('selectCommaWrong', ()=>{
        currentPart = 3;
        menu.press(3)
        partOn();

        window.dataLayer = window.dataLayer || [];
        dataLayer.push({
            event: 'link_button',
            pagePath: 'slide_0',
            slide_title: 'vitrum - 0',
            anchor: 'anchor-01',
            project: 'vitrum',
            mode: 'flat',
            version: 'd',
            link_button: 'comma2_click'
        })
        dataLayer.push({
            event: 'pageview',
            pagePath: 'slide_' + currentPart,
            slide_title: 'vitrum - ' + currentPart,
            anchor: 'anchor-01',
            project: 'vitrum',
            mode: 'flat',
            version: 'd'
        })
    })    

    product = new Product();
    document.querySelector('.content').appendChild(product.elem);
    product.init();
    product.anc();
    
    info = new Info();
    document.querySelector('.content').appendChild(info.elem);
    info.init();
    info.anc();
    
    test = new Test();
    document.querySelector('.content').appendChild(test.elem);
    test.on('changePart', (id)=>{
        currentPart = id;
        menu.press(id)
        partOn();

        window.dataLayer = window.dataLayer || [];
        dataLayer.push({
            event: 'pageview',
            pagePath: 'slide_' + currentPart,
            slide_title: 'vitrum - ' + currentPart,
            anchor: 'anchor-01',
            project: 'vitrum',
            mode: 'flat',
            version: 'd'
        })
    })
    test.init();

    $("#promocode1").on('click', (e)=>{
        window.dataLayer = window.dataLayer || [];
        dataLayer.push({
            event: 'link_button',
            pagePath: 'slide_1',
            slide_title: 'vitrum - 1',
            anchor: 'anchor-01',
            project: 'vitrum',
            mode: 'flat',
            version: 'd',
            link_button: 'promocode1'
        })
    })

    $("#promocode2").on('click', (e)=>{
        window.dataLayer = window.dataLayer || [];
        dataLayer.push({
            event: 'link_button',
            pagePath: 'slide_1',
            slide_title: 'vitrum - 1',
            anchor: 'anchor-01',
            project: 'vitrum',
            mode: 'flat',
            version: 'd',
            link_button: 'promocode2'
        })
    })
    
    
    parts.push(intro);
    parts.push(product);
    parts.push(info);
    parts.push(test);

    partOn();
    
    function partOff(){
        for(let part of parts){
            part.off()    
        }
    }
    
    function partOn(){
        partOff();
        parts[currentPart].onn()
    }

    window.addEventListener('resize', onResize, false);
    onResize();
    setTimeout(onResize, 1000);
}

function ga1(cp, id){
    window.dataLayer = window.dataLayer || [];

    dataLayer.push({
        event: 'link_button',
        pagePath: 'slide_' + cp,
        slide_title: 'vitrum - ' + cp,
        anchor: 'anchor-01',
        project: 'vitrum',
        mode: 'flat',
        version: 'd',
        link_button: 'menu' + id + '_click'
    })

    id = (id!=4) ? id : 1
    dataLayer.push({
        event: 'inner_link',
        pagePath: 'slide_' + cp,
        slide_title: 'vitrum - ' + cp,
        anchor: 'anchor-01',
        project: 'vitrum',
        mode: 'flat',
        version: 'd',
        inner_link: 'slide_' + id  //идентификатор ссылки
    });

    dataLayer.push({
        event: 'pageview',
        pagePath: 'slide_' + id,
        slide_title: 'vitrum - ' + id,
        anchor: 'anchor-01',
        project: 'vitrum',
        mode: 'flat',
        version: 'd'
    })
}



function onResize(){
    let wd = window.innerWidth;
    let hg = window.innerHeight;
    
    // Устоять нельзя свалиться
    $('.text3').find('svg').attr('width', (wd - 30) + 'px');
    
    if(hg - (45 + 80) < 650){
        $('.test').css('top', '0').css('transform', 'translate(0, 0)')
    }else{
        $('.test').css('top', '50%').css('transform', 'translate(0, -50%)')
    }

    if(hg - (45 + 80) < 442){
        $('.intro').css('top', '0').css('transform', 'translate(0, 0)')
    }else{
        $('.intro').css('top', '50%').css('transform', 'translate(0, -50%)')
    }    
}

document.addEventListener("DOMContentLoaded", ready);















