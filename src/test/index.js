'use strict';

import './test.styl';
import template from './test.jade'
import { EventEmitter } from 'events'
import $ from 'jquery'

const TweenMax = require('TweenMax');

let questionNumber;
let summ;
let question, answer, success, unsuccess, successimage, hurts;
let that;
let svg;

const color_orange = '#e88940';
const color_gray = '#e9e9e9';
const color_gray2 = '#919496';

export default class Test extends EventEmitter{
    constructor(options) {
        super();
        
        this.elem = document.createElement('div');
        this.elem.className = 'test';
        this.elem.innerHTML = template();
    }

    init(){
        
        that = this;
        svg = $(this.elem).find('.turns').find('svg');
        question = $(this.elem).find('.questionblock');
        answer = $(this.elem).find('.answerblock');
        success = $(this.elem).find('.successblock');
        successimage = $(this.elem).find('.success_image');
        unsuccess = $(this.elem).find('.unsuccessblock');
        hurts = $(this.elem).find('.hurts');
        
        question.find('#answer0').on('click', (e)=>{
            that.answerQuestion(0);
        })
        question.find('#answer1').on('click', (e)=>{
            that.answerQuestion(1);
        })
        answer.find('.nextquestion').on('click', (e)=>{
            that.nextQuestion();
        })
        unsuccess.find('.unsuccess_btn').on('click', (e)=>{
            that.emit('changePart', 1)
            that.restart()

            window.dataLayer = window.dataLayer || [];
            dataLayer.push({
                event: 'link_button',
                pagePath: 'slide_3',
                slide_title: 'vitrum - 03',
                anchor: 'anchor-01',
                project: 'vitrum',
                mode: 'flat',
                version: 'd',
                link_button: 'knowHowFail_click'
            })
            
            
        })
        success.find('.success_btn').on('click', (e)=>{
            that.emit('changePart', 1)
            that.restart()

            window.dataLayer = window.dataLayer || [];
            dataLayer.push({
                event: 'link_button',
                pagePath: 'slide_3',
                slide_title: 'vitrum - 03',
                anchor: 'anchor-01',
                project: 'vitrum',
                mode: 'flat',
                version: 'd',
                link_button: 'knowHowWin_click'
            })
        })
        unsuccess.find('span').on('click', (e)=>{
            that.restart()

            window.dataLayer = window.dataLayer || [];
            dataLayer.push({
                event: 'link_button',
                pagePath: 'slide_3',
                slide_title: 'vitrum - 03',
                anchor: 'anchor-01',
                project: 'vitrum',
                mode: 'flat',
                version: 'd',
                link_button: 'oneMore_click'
            })
        })
        
        that.restart();
    }
    
    restart(){
        questionNumber = -1;
        summ = 0;
        $('.hurt').find('svg').find('#bg').attr('fill', color_orange);
        $('.testblock').removeClass('testcenter')
        hurts.show()
        $('.turns').show();
        success.hide();
        unsuccess.hide();
        successimage.hide();
        that.nextQuestion();
    }
    
    nextQuestion(){
        if(summ < 3){
            questionNumber++;
           
            if(questionNumber > 8){
                success.fadeIn();
                successimage.fadeIn();
                hurts.fadeOut()
                answer.hide();
                $('.turns').hide();
                $('.testblock').addClass('testcenter')

                window.dataLayer = window.dataLayer || [];
                dataLayer.push({
                    event : 'generic',
                    pagePath: 'slide_3',
                    slide_title: 'vitrum - 03',
                    anchor: 'anchor-01',
                    object: 'win_show',
                    object_event: 'win_show'                                                                 // Название события
                })


            }else{
                svg.find('#location0').find('path').attr('fill', color_gray2);
                svg.find('#location0').find('polygon').attr('fill', color_gray2);
                svg.find('#location1').find('path').attr('fill', color_gray2);
                svg.find('#location1').find('polygon').attr('fill', color_gray2);
                svg.find('#location2').find('path').attr('fill', color_gray2);
                svg.find('#location2').find('polygon').attr('fill', color_gray2);
    
                for(let i=1; i<=9; i++){
                    svg.find('#sq'+i).attr('stroke-width', 1)
                    if(i==(questionNumber+1)){
                        svg.find('#n'+i).attr('fill', '#fff');
                        svg.find('#sq'+i).attr('fill', color_orange);
                        svg.find('#sq'+i).attr('stroke', color_orange);
                    }else if(i < (questionNumber+1)){
                        svg.find('#n'+i).attr('fill', '#fff');
                        svg.find('#sq'+i).attr('fill', color_gray2);
                        svg.find('#sq'+i).attr('stroke', color_gray2);
                    }else{
                        svg.find('#n'+i).attr('fill', '#000');
                        svg.find('#sq'+i).attr('fill', '#fff');
                        svg.find('#sq'+i).attr('stroke', color_orange);
                    }
                }
    
                if(questionNumber < 3){
                    svg.find('#location0').find('path').attr('fill', color_orange);
                    svg.find('#location0').find('polygon').attr('fill', color_orange);
                    $('.turns').find('img').attr('src', 'static/img/test0.png')
                }else if(questionNumber < 6){
                    svg.find('#location1').find('path').attr('fill', color_orange);
                    svg.find('#location1').find('polygon').attr('fill', color_orange);
                    $('.turns').find('img').attr('src', 'static/img/test1.png')
                }else{
                    svg.find('#location2').find('path').attr('fill', color_orange);
                    svg.find('#location2').find('polygon').attr('fill', color_orange);
                    $('.turns').find('img').attr('src', 'static/img/test2.png')
                }
    
                that.askQuestion();                
            }
    
    
        }else{
            unsuccess.fadeIn();
            hurts.fadeOut()
            answer.hide();
            $('.turns').hide();
            $('.testblock').addClass('testcenter')

            window.dataLayer = window.dataLayer || [];
            dataLayer.push({
                event : 'generic',
                pagePath: 'slide_3',
                slide_title: 'vitrum - 03',
                anchor: 'anchor-01',
                object: 'fail_show',
                object_event: 'fail_show'                                                                 // Название события
            })
        }
    }
    
    askQuestion(){
        question.fadeIn();
        answer.hide();
    
        question.find('.question').html(window.testData[questionNumber].question);
        question.find('#answer0').html(window.testData[questionNumber].answer0);
        question.find('#answer1').html(window.testData[questionNumber].answer1);

        window.dataLayer = window.dataLayer || [];
        dataLayer.push({
            event : 'generic',
            pagePath: 'slide_3',
            slide_title: 'vitrum - 03',
            anchor: 'anchor-01',
            object: 'question',
            object_event: 'question' + questionNumber                                                                 // Название события
        })
    }
    
    answerQuestion(n){
        answer.fadeIn();
        question.hide();
    
        let answerText = window.testData[questionNumber]['text'+n]
        let hedr = '';
        let answ = '';
        let position = answerText.search(/[.!?]/)
        hedr = answerText.substring(0, position+1);
        answ = answerText.substring(position+1)
    
        answer.find('.answerHeader').html(hedr);
        answer.find('.answerText').html(answ);

        window.dataLayer = window.dataLayer || [];

        if(n != window.testData[questionNumber].code){
            answer.find('.answerHeader').removeClass('right').addClass('wrong')
            summ++;
            $('#hurt0').find('svg').find('#bg').attr('fill', (summ<1) ? color_orange : color_gray2)
            $('#hurt1').find('svg').find('#bg').attr('fill', (summ<2) ? color_orange : color_gray2)
            $('#hurt2').find('svg').find('#bg').attr('fill', (summ<3) ? color_orange : color_gray2)

            dataLayer.push({
                event: 'link_button',
                pagePath: 'slide_3',
                slide_title: 'vitrum - 03',
                anchor: 'anchor-01',
                project: 'vitrum',
                mode: 'flat',
                version: 'd',
                link_button: 'answerWrong' + questionNumber
            })

        }else{
            answer.find('.answerHeader').addClass('right').removeClass('wrong')

            dataLayer.push({
                event: 'link_button',
                pagePath: 'slide_3',
                slide_title: 'vitrum - 03',
                anchor: 'anchor-01',
                project: 'vitrum',
                mode: 'flat',
                version: 'd',
                link_button: 'answerRight' + questionNumber
            })
        }
    }
    
    onn(){
        $(this.elem).fadeIn();
    }
    
    off(){
        $(this.elem).hide();
    }
};