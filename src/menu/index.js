'use strict';

import './menu.styl';
import template from './menu.jade'
import { EventEmitter } from 'events'
import $ from 'jquery'

const TweenMax = require('TweenMax');

export default class Menu extends EventEmitter{
    constructor(options) {
        super();

        this.elem = document.createElement('div');
        this.elem.className = 'menu';
        this.elem.innerHTML = template();
    }

    init(n){

        let btn = $('.btn')
        for(let i=0; i<btn.length; i++){
            $(btn[i]).data('id', i)
            if(i==n){
                $(btn[i]).addClass('pressed');
            }            
        }
        
        $('.btn').on('mouseover', (e)=>{
            $(e.target).addClass('hovered')
        })
        $('.btn').on('mouseout', (e)=>{
            $(e.target).removeClass('hovered')
        })
        $('.btn').on('click', (e)=>{
            $('.btn').removeClass('hovered')
            $('.btn').removeClass('pressed')
            $(e.target).addClass('pressed')
            
            this.emit('changePart', $(e.target).data('id'))
        })
    }
    
    press(n){
        $('.btn').removeClass('hovered')
        $('.btn').removeClass('pressed')

        let btn = $('.btn')
        for(let i=0; i<btn.length; i++){
            if($(btn[i]).data('id') == n)
                $(btn[i]).addClass('pressed')
        }
        
    }
};