'use strict';

import './product.styl';
import template from './product.jade'
import { EventEmitter } from 'events'
import $ from 'jquery'
import Promo from './../promo'

export default class Product extends EventEmitter{
    constructor(options) {
        super();

        this.elem = document.createElement('div');
        this.elem.className = 'product';
        this.elem.innerHTML = template();
    }

    init(){
        let vitamins = $('.pr_vitamin');
        let popup = $('.pr_popup');
        popup.fadeTo(0, 0)
        let promo = new Promo();
        document.querySelector('.product').appendChild(promo.elem);
        for(let i=0; i<vitamins.length; i++){
            $(vitamins[i]).data('id', i)
        }

        let id = 0;
        popup.fadeTo(0, 0)
        popup.find('.vit_name').text(window.productData[id].name);
        popup.find('.vit_name').css('background-color', $('#vit'+id).css('background-color'));
        popup.find('.vit_text').text(window.productData[id].txt);
        popup.find('.vit_img').find('img').attr('src', 'static/img/v'+id+'.png');
        let wdd = popup.innerWidth()
        let dx = (wdd - 660) /2
        let wddx = (wdd - 2*dx) / 5
        popup.find('.vit_angle').css('left', 50 + dx + wddx * id + 'px')
        popup.fadeTo(300, 1)
        
        vitamins.on('click', (e)=>{
            let id = $(e.target).data('id');
            popup.fadeTo(0, 0)
            popup.find('.vit_name').text(window.productData[id].name);
            popup.find('.vit_name').css('background-color', $('#vit'+id).css('background-color'));
            popup.find('.vit_text').text(window.productData[id].txt);
            popup.find('.vit_img').find('img').attr('src', 'static/img/v'+id+'.png');
            let wdd = popup.innerWidth()
            let dx = (wdd - 660) /2
            let wddx = (wdd - 2*dx) / 5
            popup.find('.vit_angle').css('left', 50 + dx + wddx * id + 'px')
            popup.fadeTo(300, 1)

            window.dataLayer = window.dataLayer || [];
            dataLayer.push({
                event: 'link_button',
                pagePath: 'slide_1',
                slide_title: 'vitrum - 01',
                anchor: 'anchor-01',
                project: 'vitrum',
                mode: 'flat',
                version: 'd',
                link_button: 'vita' + id + '_click'
            })
        });



    }

    anc(){
        let once = false;
        $(document).scroll(function () {
            anchor();
        });
        anchor();

        function anchor(){
            let s_top = $("body").scrollTop();
            let yes = $(".promo").offset().top;
            let hg = window.innerHeight;
            if(hg + s_top > yes){
                if(!once){
                    once = true
                    window.dataLayer = window.dataLayer || [];
                    dataLayer.push({
                        event : 'generic',
                        pagePath: 'slide_1',
                        slide_title: 'vitrum - 01',
                        anchor: 'anchor-01',
                        object: 'anchor0201_show',
                        object_event: 'anchor0201_show'
                    })

                }
            }
        }        
    }
    
    onn(){
        $(this.elem).fadeIn()
    }

    off(){
        $(this.elem).hide()
    }
};