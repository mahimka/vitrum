'use strict';

import './intro.styl';
import template from './intro.jade'
import { EventEmitter } from 'events'
import $ from 'jquery'

export default class Intro extends EventEmitter{
    constructor(options) {
        super();

        this.elem = document.createElement('div');
        this.elem.className = 'intro';
        this.elem.innerHTML = template();
    }

    init(){
        let svg = $(this.elem).find('svg');
        let timeout;
        let that = this

        $('#imagewrong').hide()
        $('#cr').hide()
        $('#cw').hide()

        svg.find('#commaWrong').on('click', ()=>{
            $('#cr').fadeOut()
            $('#cw').fadeIn()

            $('#imagewrong').fadeIn()
            $('#imageright').hide()

            clearTimeout(timeout)
            timeout = window.setTimeout(()=>{
                that.emit('selectCommaWrong');
            }, 1000)

        })
        svg.find('#commaRight').on('click', ()=>{
            $('#cr').fadeIn()
            $('#cw').fadeOut()

            $('#imagewrong').hide()
            $('#imageright').fadeIn()

            clearTimeout(timeout)
            timeout = window.setTimeout(()=>{
                that.emit('selectCommaRight');
            }, 1000)

        })
    }
    
    onn(){
        $(this.elem).fadeIn()        
    }
    
    off(){
        $(this.elem).hide()
    }
};